Attribute VB_Name = "MasterArrival_Process_Module"
Option Explicit
Public g_strConnection As String
Public cnn As ADODB.Connection

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "BBC Media Portal at JCA TV"
Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Private Declare Function OpenProcess Lib "kernel32" ( _
    ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long
Private Declare Function EnumProcesses Lib "PSAPI.DLL" ( _
   lpidProcess As Long, ByVal cb As Long, cbNeeded As Long) As Long
Private Declare Function EnumProcessModules Lib "PSAPI.DLL" ( _
    ByVal hProcess As Long, lphModule As Long, ByVal cb As Long, lpcbNeeded As Long) As Long
Private Declare Function GetModuleBaseName Lib "PSAPI.DLL" Alias "GetModuleBaseNameA" ( _
    ByVal hProcess As Long, ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
Private Const PROCESS_VM_READ = &H10
Private Const PROCESS_QUERY_INFORMATION = &H400
Public Const TC_UN = 0
Public Const TC_25 = 1
Public Const TC_29 = 2
Public Const TC_30 = 3
Public Const TC_24 = 4

Public Sub MasterArrivalProcess()

Dim l_rsJobs As ADODB.Recordset, l_rsCompanies As ADODB.Recordset, l_rsMedia As ADODB.Recordset, l_strSQL As String, l_blnMediaHere As Boolean, l_rsWoolySearch As ADODB.Recordset
Dim l_strLocation As String, l_strShelf As String, l_rsTapeMovement As ADODB.Recordset, l_lngLibraryID As Long, SQL As String, l_lngEventID As Long, l_strAltLocation As String, BBCMG_OrderID As Long, BBCMG_MediaKey As String
Dim l_rstTapes As ADODB.Recordset, l_strTemp As String, l_rsTracker As ADODB.Recordset, l_lngChannelCount As Long
Dim l_rsDADCTracker As ADODB.Recordset, l_strBarcode As String, l_strEmailBody As String, l_strTapeFormat As String, l_rst As ADODB.Recordset, l_datNewTargetDate As Date, l_datNewTargetDate2 As Date
Dim l_strPathToFile As String, FSO As Scripting.FileSystemObject, Count As Long, Framerate As Integer, l_strDuration As String, l_lngDuration As Long
Dim l_rsTurnerTracker As ADODB.Recordset, l_strNewFolder As String, l_rsTurnerTracker2 As ADODB.Recordset
Dim l_lngRunningTime As Long, l_lngTurnerLibraryID As Long
Dim LockFlag As Long

cnn.Open g_strConnection

LockFlag = GetData("setting", "value", "name", "lockSystem")
If (LockFlag And 16) = 16 Then
    cnn.Close
    Exit Sub
End If

Set l_rsCompanies = New ADODB.Recordset
Set l_rsJobs = New ADODB.Recordset
Set l_rsMedia = New ADODB.Recordset
Set l_rsWoolySearch = New ADODB.Recordset
Set l_rstTapes = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rsCompanies.Open SQL, cnn, 3, 3
If l_rsCompanies.RecordCount > 0 Then
    g_strUserEmailAddress = l_rsCompanies("value")
End If
l_rsCompanies.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rsCompanies.Open SQL, cnn, 3, 3
If l_rsCompanies.RecordCount > 0 Then
    g_strUserEmailName = l_rsCompanies("value")
End If
l_rsCompanies.Close

'DADC (BBC) Tracker Tape checks

'First check all the tapes that haven't arrived, to see whether they have, and mark stagefield one if they have.
l_strEmailBody = ""
Set l_rst = New ADODB.Recordset
App.LogEvent "MasterArrivalService: Commencing DADC (BBC) Tracker Tape Checks"
l_strSQL = "SELECT newbarcode, title, episode, stagefield1, stagefield14, companyID, format, duedateupload, companyID, tapelocation, tapeshelf, rejected, readytobill, completeable, targetdatecountdown, targetdate, firstcompleteable, mdate "
l_strSQL = l_strSQL & "FROM tracker_dadc_item WHERE newbarcode IS NOT NULL and newbarcode <> '' AND readytobill = 0 AND billed = 0 and ((stagefield1 IS NULL) OR (stagefield1 IS NOT NULL AND stagefield14 IS NOT NULL));"
Set l_rsDADCTracker = New ADODB.Recordset
Set l_rsTapeMovement = New ADODB.Recordset
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    While Not l_rsDADCTracker.EOF
        l_strBarcode = l_rsDADCTracker("newbarcode")
        Debug.Print l_strBarcode
        'Correct the tape format entry in the tracker
        If UCase(Right(l_strBarcode, 5)) = "_REDO" Then l_strBarcode = Left(l_strBarcode, Len(l_strBarcode) - 5)
        l_strTapeFormat = GetData("library", "format", "barcode", l_strBarcode)
        If Trim(" " & l_rsDADCTracker("format")) <> l_strTapeFormat And l_strTapeFormat <> "" Then
            l_rsDADCTracker("format") = l_strTapeFormat
            l_rsDADCTracker.Update
        End If
        If GetData("library", "libraryID", "barcode", l_strBarcode) <> 0 Then
            l_lngLibraryID = GetData("library", "libraryID", "barcode", l_strBarcode)
            l_strLocation = GetData("library", "location", "barcode", l_strBarcode)
            l_strShelf = GetData("library", "shelf", "barcode", l_strBarcode)
            If IsItHere(l_strLocation) = True Then
                l_rsTapeMovement.Open "SELECT cdate FROM trans WHERE libraryID = " & l_lngLibraryID & " ORDER by cdate DESC;", cnn, 3, 3
                If l_rsTapeMovement.RecordCount > 0 Then
                    l_rsTapeMovement.MoveFirst
                    l_rsDADCTracker("stagefield1") = l_rsTapeMovement("cdate")
                    If l_rsDADCTracker("stagefield1") <> "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                    
                        Set l_rst = New ADODB.Recordset
                        l_rst.Open "exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(l_rsDADCTracker("targetdatecountdown")), cnn
                        l_datNewTargetDate = l_rst(0)
                        l_rst.Close
                        Set l_rst = Nothing
                        If Trim(" " & l_rsDADCTracker("Completeable")) = "" Then l_rsDADCTracker("Completeable") = Now()
                        If Trim(" " & l_rsDADCTracker("firstcompleteable")) = "" Then l_rsDADCTracker("firstCompleteable") = l_rsDADCTracker("Completeable")
                        If Trim(" " & l_rsDADCTracker("TargetDate")) = "" Then l_rsDADCTracker("TargetDate") = l_datNewTargetDate
                    
                    ElseIf l_rsDADCTracker("readytobill") = 0 Then
                    
                        l_rsDADCTracker("Completeable") = Null
                        l_rsDADCTracker("TargetDate") = Null
                            
                    End If
                    
                Else
                    If Trim(" " & GetData("library", "cdate", "libraryID", l_lngLibraryID)) <> "" Then
                        l_rsDADCTracker("stagefield1") = GetData("library", "cdate", "libraryID", l_lngLibraryID)
                        If l_rsDADCTracker("stagefield1") <> "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                        
                            Set l_rst = New ADODB.Recordset
                            l_rst.Open "exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(l_rsDADCTracker("targetdatecountdown")), cnn
                            l_datNewTargetDate = l_rst(0)
                            l_rst.Close
                            Set l_rst = Nothing
                            If Trim(" " & l_rsDADCTracker("Completeable")) = "" Then l_rsDADCTracker("Completeable") = Now()
                            If Trim(" " & l_rsDADCTracker("firstcompleteable")) = "" Then l_rsDADCTracker("firstCompleteable") = l_rsDADCTracker("Completeable")
                            If Trim(" " & l_rsDADCTracker("TargetDate")) = "" Then l_rsDADCTracker("TargetDate") = l_datNewTargetDate
                        
                        ElseIf l_rsDADCTracker("readytobill") = 0 Then
                        
                            l_rsDADCTracker("Completeable") = Null
                            l_rsDADCTracker("TargetDate") = Null
                                
                        End If
                            
                    Else
                        l_rsDADCTracker("stagefield1") = Format(Now, "dd/mm/yyyy hh:nn:ss")
                        If l_rsDADCTracker("stagefield1") <> "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                        
                            Set l_rst = New ADODB.Recordset
                            l_rst.Open "exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(l_rsDADCTracker("targetdatecountdown")), cnn
                            l_datNewTargetDate = l_rst(0)
                            l_rst.Close
                            Set l_rst = Nothing
                            If Trim(" " & l_rsDADCTracker("Completeable")) = "" Then l_rsDADCTracker("Completeable") = Now()
                            If Trim(" " & l_rsDADCTracker("firstcompleteable")) = "" Then l_rsDADCTracker("firstCompleteable") = l_rsDADCTracker("Completeable")
                            If Trim(" " & l_rsDADCTracker("TargetDate")) = "" Then l_rsDADCTracker("TargetDate") = l_datNewTargetDate
                        
                        ElseIf l_rsDADCTracker("readytobill") = 0 Then
                        
                            l_rsDADCTracker("Completeable") = Null
                            l_rsDADCTracker("TargetDate") = Null
                                
                        End If
                        
                    End If
                End If
                l_rsTapeMovement.Close
                l_rsDADCTracker("stagefield14") = Null
                l_rsDADCTracker("tapelocation") = l_strLocation
                l_rsDADCTracker("tapeshelf") = l_strShelf
                l_rsDADCTracker("mdate") = Now
                l_rsDADCTracker.Update
                l_strEmailBody = l_strEmailBody & l_strBarcode & " - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID")) & " " & l_rsDADCTracker("title") & " " & l_rsDADCTracker("episode")
                If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                    l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
                End If
                l_strEmailBody = l_strEmailBody & vbCrLf
            End If
        End If
        l_rsDADCTracker.MoveNext
    Wend
End If
l_rsDADCTracker.Close

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following tapes that you have just booked in are required for BBC DADC work:" & vbCrLf & vbCrLf & l_strEmailBody
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 16;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "BBC DADC Tapes just booked in", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

'Then check all the files that haven't arrived, to see whether they have, and mark stagefield one if they have.
Dim l_strFilename As String, l_strReference As String, l_strNewFilename As String, l_strNewReference As String, l_lngClipID As Long, l_strNewPathToFile As String, l_strNotMXFViewName As String, l_strMXFViewName As String
Dim SanitisedReference As String
l_strEmailBody = ""
Set l_rst = New ADODB.Recordset
App.LogEvent "MasterArrivalService: Commencing DADC (BBC) Tracker File Checks"
l_strSQL = "SELECT newbarcode, contentversioncode, contentversionID, workflowID, title, episode, stagefield1, stagefield2, stagefield14, stagefield5, companyID, format, duedateupload, companyID, tapelocation, tapeshelf, "
l_strSQL = l_strSQL & "rejected, readytobill, completeable, targetdatecountdown, targetdate, firstcompleteable, mdate, itemreference "
l_strSQL = l_strSQL & "FROM tracker_dadc_item WHERE newbarcode IS NOT NULL and newbarcode <> '' AND readytobill = 0 AND billed = 0 and stagefield1 IS NULL;"
Set l_rsDADCTracker = New ADODB.Recordset
Set l_rsTapeMovement = New ADODB.Recordset
Set FSO = New Scripting.FileSystemObject
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    Debug.Print l_rsDADCTracker.RecordCount
    l_rsDADCTracker.MoveFirst
    Do While Not l_rsDADCTracker.EOF
        l_strReference = DADCSanitiseBarcode(l_rsDADCTracker("newbarcode"))
        Debug.Print l_strReference
        DoEvents
        Select Case l_rsDADCTracker("companyID")
            Case 1261
                l_strNotMXFViewName = "vw_EventsFor1261notMXF"
                l_strMXFViewName = "vw_EventsFor1261MXF"
            Case 1239
                l_strNotMXFViewName = "vw_EventsFor1239notMXF"
                l_strMXFViewName = "vw_EventsFor1239MXF"
            Case Else
                Exit Sub
        End Select
        SanitisedReference = QuoteSanitise(l_strReference)
'        If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '" & SanitisedReference & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%')")) <> "" Then
        If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '%" & SanitisedReference & "%') AND clipfilename LIKE '%.mov'")) <> "" Then
            l_lngClipID = GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '" & QuoteSanitise(l_strReference) & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%') AND clipfilename LIKE '%.mov'")
            l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
            l_strFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
            l_strNewReference = l_rsDADCTracker("itemreference")
            l_strNewFilename = l_strNewReference & Right(GetData("events", "clipfilename", "eventID", l_lngClipID), 4)
            l_strPathToFile = GetData("library", "subtitle", "libraryID", l_lngLibraryID)
            If GetData("events", "altlocation", "eventID", l_lngClipID) <> "" Then l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
            If FSO.FileExists(l_strPathToFile & "\" & l_strFilename) And Not FSO.FileExists(l_strPathToFile & "\" & l_strNewFilename) Then
'                FSO.MoveFile l_strPathToFile & "\" & l_strFilename, l_strPathToFile & "\" & l_strNewFilename
'                SetData "events", "clipfilename", "eventID", l_lngClipID, l_strNewFilename
'                SetData "events", "clipreference", "eventID", l_lngClipID, l_strNewReference
                If Trim(" " & l_rsDADCTracker("stagefield1")) = "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                    l_rsDADCTracker("stagefield1") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                    l_rsDADCTracker("stagefield2") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                    If l_rsDADCTracker("stagefield1") <> "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                        Set l_rst = New ADODB.Recordset
                        l_rst.Open "exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(l_rsDADCTracker("targetdatecountdown")), cnn
                        l_datNewTargetDate = l_rst(0)
                        l_rst.Close
                        Set l_rst = Nothing
                        If Trim(" " & l_rsDADCTracker("Completeable")) = "" Then l_rsDADCTracker("Completeable") = Now()
                        If Trim(" " & l_rsDADCTracker("firstcompleteable")) = "" Then l_rsDADCTracker("firstCompleteable") = l_rsDADCTracker("Completeable")
                        If Trim(" " & l_rsDADCTracker("TargetDate")) = "" Then l_rsDADCTracker("TargetDate") = l_datNewTargetDate
                    ElseIf l_rsDADCTracker("readytobill") = 0 Then
                        l_rsDADCTracker("Completeable") = Null
                        l_rsDADCTracker("TargetDate") = Null
                    End If
                    If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strMXFViewName & " WHERE (clipreference LIKE '" & QuoteSanitise(l_strReference) & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%')")) <> "" Then
                        l_rsDADCTracker("stagefield5") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                    Else
                        l_rsDADCTracker("stagefield5") = Null
                    End If
                    l_rsDADCTracker("stagefield14") = Null
                    l_rsDADCTracker("format") = "FILE"
                    l_rsDADCTracker("mdate") = Now
                    l_rsDADCTracker.Update
                    l_strEmailBody = l_strEmailBody & l_strReference & " - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID")) & " " & l_rsDADCTracker("title") & " " & l_rsDADCTracker("episode")
                    If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                        l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
                    End If
                    l_strEmailBody = l_strEmailBody & vbCrLf
                    l_strSQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, NewFileName, DoNotRecordCopy, bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngClipID & ", "
                    l_strSQL = l_strSQL & l_lngLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                    l_strSQL = l_strSQL & GetData("setting", "intvalue", "name", "DADC_Store_Archive_ID") & ", "
                    l_strSQL = l_strSQL & "'1261', "
                    l_strSQL = l_strSQL & "'" & l_strNewFilename & "', 1, "
                    l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventid", l_lngClipID) & ", "
                    l_strSQL = l_strSQL & "'System', '" & g_strUserEmailAddress & "'"
                    l_strSQL = l_strSQL & ")"
                    Debug.Print l_strSQL
                    cnn.Execute l_strSQL
                End If
            End If
        End If
        l_rsDADCTracker.MoveNext
    Loop
End If
l_rsDADCTracker.Close

Set l_rst = New ADODB.Recordset
App.LogEvent "MasterArrivalService: Commencing DADC (BBC) Tracker File Checks"
l_strSQL = "SELECT newbarcode, contentversioncode, contentversionID, workflowID, title, episode, stagefield1, stagefield2, stagefield14, stagefield5, companyID, format, duedateupload, companyID, tapelocation, tapeshelf, "
l_strSQL = l_strSQL & "rejected, readytobill, completeable, targetdatecountdown, targetdate, firstcompleteable, mdate, itemreference "
l_strSQL = l_strSQL & "FROM tracker_dadc_item WHERE newbarcode IS NOT NULL and newbarcode <> '' AND readytobill = 0 AND billed = 0 and stagefield1 IS NULL;"
Set l_rsDADCTracker = New ADODB.Recordset
Set l_rsTapeMovement = New ADODB.Recordset
Set FSO = New Scripting.FileSystemObject
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    Debug.Print l_rsDADCTracker.RecordCount
    l_rsDADCTracker.MoveFirst
    Do While Not l_rsDADCTracker.EOF
        l_strReference = DADCSanitiseBarcode(l_rsDADCTracker("itemreference"))
        Debug.Print l_strReference
        DoEvents
        Select Case l_rsDADCTracker("companyID")
            Case 1261
                l_strNotMXFViewName = "vw_EventsFor1261notMXF"
                l_strMXFViewName = "vw_EventsFor1261MXF"
            Case 1239
                l_strNotMXFViewName = "vw_EventsFor1239notMXF"
                l_strMXFViewName = "vw_EventsFor1239MXF"
            Case Else
                Exit Sub
        End Select
        SanitisedReference = QuoteSanitise(l_strReference)
'        If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '" & SanitisedReference & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%')")) <> "" Then
        If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '%" & SanitisedReference & "%') AND clipfilename LIKE '%.mov'")) <> "" Then
            l_lngClipID = GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '" & QuoteSanitise(l_strReference) & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%') AND clipfilename LIKE '%.mov'")
            l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
            l_strFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
            l_strNewReference = l_rsDADCTracker("itemreference")
            l_strNewFilename = l_strNewReference & Right(GetData("events", "clipfilename", "eventID", l_lngClipID), 4)
            l_strPathToFile = GetData("library", "subtitle", "libraryID", l_lngLibraryID)
            If GetData("events", "altlocation", "eventID", l_lngClipID) <> "" Then l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
            If FSO.FileExists(l_strPathToFile & "\" & l_strFilename) And Not FSO.FileExists(l_strPathToFile & "\" & l_strNewFilename) Then
                FSO.MoveFile l_strPathToFile & "\" & l_strFilename, l_strPathToFile & "\" & l_strNewFilename
                SetData "events", "clipfilename", "eventID", l_lngClipID, l_strNewFilename
                SetData "events", "clipreference", "eventID", l_lngClipID, l_strNewReference
            End If
            If Trim(" " & l_rsDADCTracker("stagefield1")) = "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                l_rsDADCTracker("stagefield1") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                l_rsDADCTracker("stagefield2") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                If l_rsDADCTracker("stagefield1") <> "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                    Set l_rst = New ADODB.Recordset
                    l_rst.Open "exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(l_rsDADCTracker("targetdatecountdown")), cnn
                    l_datNewTargetDate = l_rst(0)
                    l_rst.Close
                    Set l_rst = Nothing
                    If Trim(" " & l_rsDADCTracker("Completeable")) = "" Then l_rsDADCTracker("Completeable") = Now()
                    If Trim(" " & l_rsDADCTracker("firstcompleteable")) = "" Then l_rsDADCTracker("firstCompleteable") = l_rsDADCTracker("Completeable")
                    If Trim(" " & l_rsDADCTracker("TargetDate")) = "" Then l_rsDADCTracker("TargetDate") = l_datNewTargetDate
                ElseIf l_rsDADCTracker("readytobill") = 0 Then
                    l_rsDADCTracker("Completeable") = Null
                    l_rsDADCTracker("TargetDate") = Null
                End If
                If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strMXFViewName & " WHERE (clipreference LIKE '" & QuoteSanitise(l_strReference) & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%')")) <> "" Then
                    l_rsDADCTracker("stagefield5") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                Else
                    l_rsDADCTracker("stagefield5") = Null
                End If
                l_rsDADCTracker("stagefield14") = Null
                l_rsDADCTracker("format") = "FILE"
                l_rsDADCTracker("mdate") = Now
                l_rsDADCTracker.Update
                l_strEmailBody = l_strEmailBody & l_strReference & " - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID")) & " " & l_rsDADCTracker("title") & " " & l_rsDADCTracker("episode")
                If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                    l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
                End If
                l_strEmailBody = l_strEmailBody & vbCrLf
                l_strSQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_lngClipID & ", "
                l_strSQL = l_strSQL & l_lngLibraryID & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                l_strSQL = l_strSQL & GetData("setting", "intvalue", "name", "DADC_Store_Archive_ID") & ", "
                l_strSQL = l_strSQL & "'1261', "
                l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventid", l_lngClipID) & ", "
                l_strSQL = l_strSQL & "'System', '" & g_strUserEmailAddress & "'"
                l_strSQL = l_strSQL & ")"
                Debug.Print l_strSQL
                cnn.Execute l_strSQL
            End If
        End If
        l_rsDADCTracker.MoveNext
    Loop
End If
l_rsDADCTracker.Close

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following files that have Arrived in are required for BBC DADC work:" & vbCrLf & vbCrLf & l_strEmailBody & vbCrLf & vbCrLf & "They have been renamed to match the IR, and CETA Requests have been submitted to move them to LAT-CETA."
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 16;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "BBC DADC Files just arrived", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

'Then check all the files that haven't arrived, to see whether they have arrived for BBCMG, and mark stagefield one if they have.
l_strEmailBody = ""
Set l_rst = New ADODB.Recordset
App.LogEvent "MasterArrivalService: Commencing DADC (BBC) Tracker File Checks for NNCMG files"
l_strSQL = "SELECT newbarcode, contentversioncode, contentversionID, workflowID, title, episode, stagefield1, stagefield2, stagefield14, stagefield5, companyID, format, duedateupload, companyID, tapelocation, tapeshelf, "
l_strSQL = l_strSQL & "rejected, readytobill, completeable, targetdatecountdown, targetdate, firstcompleteable, mdate, itemreference "
l_strSQL = l_strSQL & "FROM tracker_dadc_item WHERE newbarcode IS NOT NULL and newbarcode <> '' AND readytobill = 0 AND billed = 0 and stagefield1 IS NULL;"
Set l_rsDADCTracker = New ADODB.Recordset
Set l_rsTapeMovement = New ADODB.Recordset
Set FSO = New Scripting.FileSystemObject
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    Debug.Print l_rsDADCTracker.RecordCount
    l_rsDADCTracker.MoveFirst
    Do While Not l_rsDADCTracker.EOF
        l_strReference = DADCSanitiseBarcode(l_rsDADCTracker("newbarcode"))
        Debug.Print l_strReference
        DoEvents
        l_strNotMXFViewName = "vw_EventsFor924notMXF"
        l_strMXFViewName = "vw_EventsFor924MXF"
        SanitisedReference = QuoteSanitise(l_strReference)
'        If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '" & SanitisedReference & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%')")) <> "" Then
        If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '%" & SanitisedReference & "%' AND clipfilename LIKE '%.mov')")) <> "" Then
            l_lngClipID = GetDataSQL("SELECT TOP 1 eventID FROM " & l_strNotMXFViewName & " WHERE (clipreference LIKE '" & QuoteSanitise(l_strReference) & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%' AND clipfilename LIKE '%.mov')")
            SetData "events", "companyID", "eventID", l_lngClipID, l_rsDADCTracker("companyID")
            SetData "events", "companyname", "eventID", l_lngClipID, "Sony DADC Ltd (Manual DBB Ingest)"
            l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
            l_strFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
            l_strNewReference = l_rsDADCTracker("itemreference")
            l_strNewFilename = l_strNewReference & Right(GetData("events", "clipfilename", "eventID", l_lngClipID), 4)
            l_strPathToFile = GetData("library", "subtitle", "libraryID", l_lngLibraryID)
            If GetData("events", "altlocation", "eventID", l_lngClipID) <> "" Then l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
            If FSO.FileExists(l_strPathToFile & "\" & l_strFilename) Then
                FSO.MoveFile l_strPathToFile & "\" & l_strFilename, l_strPathToFile & "\" & l_strNewFilename
                SetData "events", "clipfilename", "eventID", l_lngClipID, l_strNewFilename
                SetData "events", "clipreference", "eventID", l_lngClipID, l_strNewReference
                If Trim(" " & l_rsDADCTracker("stagefield1")) = "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                    l_rsDADCTracker("stagefield1") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                    l_rsDADCTracker("stagefield2") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                    If l_rsDADCTracker("stagefield1") <> "" And Val(l_rsDADCTracker("rejected")) = 0 And l_rsDADCTracker("readytobill") = 0 Then
                        Set l_rst = New ADODB.Recordset
                        l_rst.Open "exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(l_rsDADCTracker("targetdatecountdown")), cnn
                        l_datNewTargetDate = l_rst(0)
                        l_rst.Close
                        Set l_rst = Nothing
                        If Trim(" " & l_rsDADCTracker("Completeable")) = "" Then l_rsDADCTracker("Completeable") = Now()
                        If Trim(" " & l_rsDADCTracker("firstcompleteable")) = "" Then l_rsDADCTracker("firstCompleteable") = l_rsDADCTracker("Completeable")
                        If Trim(" " & l_rsDADCTracker("TargetDate")) = "" Then l_rsDADCTracker("TargetDate") = l_datNewTargetDate
                    ElseIf l_rsDADCTracker("readytobill") = 0 Then
                        l_rsDADCTracker("Completeable") = Null
                        l_rsDADCTracker("TargetDate") = Null
                    End If
                    If Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM " & l_strMXFViewName & " WHERE (clipreference LIKE '" & QuoteSanitise(l_strReference) & "%' OR clipreference LIKE '%" & QuoteSanitise(l_strReference) & "%')")) <> "" Then
                        l_rsDADCTracker("stagefield5") = Format(Now, "YYYY-MM-DD hh:nn:ss")
                    Else
                        l_rsDADCTracker("stagefield5") = Null
                    End If
                    l_rsDADCTracker("stagefield14") = Null
                    l_rsDADCTracker("format") = "FILE"
                    l_rsDADCTracker("mdate") = Now
                    l_rsDADCTracker.Update
                    l_strEmailBody = l_strEmailBody & l_strReference & " - " & GetData("company", "name", "companyID", l_rsDADCTracker("companyID")) & " " & l_rsDADCTracker("title") & " " & l_rsDADCTracker("episode")
                    If Not IsNull(l_rsDADCTracker("duedateupload")) Then
                        l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedateupload")
                    End If
                    l_strEmailBody = l_strEmailBody & vbCrLf
                    l_strSQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngClipID & ", "
                    l_strSQL = l_strSQL & l_lngLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                    l_strSQL = l_strSQL & GetData("setting", "intvalue", "name", "DADC_Store_Archive_ID") & ", "
                    l_strSQL = l_strSQL & "'1261', "
                    l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventid", l_lngClipID) & ", "
                    l_strSQL = l_strSQL & "'System', '" & g_strUserEmailAddress & "'"
                    l_strSQL = l_strSQL & ")"
                    Debug.Print l_strSQL
                    cnn.Execute l_strSQL
                End If
            End If
        End If
        l_rsDADCTracker.MoveNext
    Loop
End If
l_rsDADCTracker.Close

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following files that have Arrived via BBCMG, and are required for BBC DADC work:" & vbCrLf & vbCrLf & l_strEmailBody & vbCrLf & vbCrLf & "They have been stolen frm BBCMG, renamed to match the IR, and CETA Requests have been submitted to move them to LAT-CETA."
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 16;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "BBC DADC Files just arrived", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

Set l_rsTapeMovement = Nothing
Set l_rsDADCTracker = Nothing

cnn.Close

End Sub

Private Function IsProcessRunning(ByVal sProcess As String) As Boolean

Const MAX_PATH As Long = 260

Dim lProcesses() As Long, lModules() As Long, N As Long, lRet As Long, hProcess As Long
Dim sName As String
        
sProcess = UCase$(sProcess)
ReDim lProcesses(1023) As Long
If EnumProcesses(lProcesses(0), 1024 * 4, lRet) Then
    For N = 0 To (lRet \ 4) - 1
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lProcesses(N))
        If hProcess Then
            ReDim lModules(1023)
            If EnumProcessModules(hProcess, lModules(0), 1024 * 4, lRet) Then
                sName = String$(MAX_PATH, vbNullChar)
                GetModuleBaseName hProcess, lModules(0), sName, MAX_PATH
                sName = Left$(sName, InStr(sName, vbNullChar) - 1)
                If Len(sName) = Len(sProcess) Then
                    If sProcess = UCase$(sName) Then IsProcessRunning = True: Exit Function
                End If
            End If
        End If
        CloseHandle hProcess
    Next N
End If
End Function

Function fb_bst(pd_dt As Date) As Boolean
  Const cs_fac = "%fb_bst, "
  Dim ls_eom As String
  If Not IsDate(pd_dt) Then fb_bst = False: Exit Function
  Select Case Month(pd_dt)
  Case 4, 5, 6, 7, 8, 9
    fb_bst = True
  Case 1, 2, 11, 12
    fb_bst = False
  Case 3
    fb_bst = False
    If Day(pd_dt) < 25 Then Exit Function
    ls_eom = "31/03/" & Year(pd_dt)
    If Weekday(pd_dt) - Weekday(ls_eom) > 0 Then Exit Function
    If Hour(pd_dt) < 1 Then Exit Function
    fb_bst = True
  Case 10
    fb_bst = True
    If Day(pd_dt) < 25 Then Exit Function
    ls_eom = "31/10/" & Year(pd_dt)
    If Weekday(pd_dt) - Weekday(ls_eom) > 0 Then Exit Function
'    If Hour( pd_dt ) < 1 Then Exit Function
    fb_bst = False
  End Select
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT TOP 1 " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & lp_varValue & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & lp_varCriterea & "'"

Set c = New ADODB.Connection
c.Open g_strConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim c As ADODB.Connection
    Dim l_strSQL As String
    Dim l_lngGetNextSequence  As Long
    Dim l_rsSequence As ADODB.Recordset
    
    Set c = New ADODB.Connection
    c.Open g_strConnection
    
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Set l_rsSequence = New ADODB.Recordset
    l_rsSequence.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
        
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = 'CFM', mdate = getdate() WHERE sequencename = '" & lp_strSequenceName & "';"
        
        c.Execute l_strSQL
    
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    c.Close
    Set c = Nothing
    
    GetNextSequence = l_lngGetNextSequence
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function Check_Reference(lp_strText As String) As Boolean

Dim l_expValidReferenceExpression As RegExp
Set l_expValidReferenceExpression = New RegExp
l_expValidReferenceExpression.Pattern = "^[a-zA-Z0-9_\s-]+$"
If l_expValidReferenceExpression.Test(lp_strText) = True Then
    Check_Reference = True
Else
    Check_Reference = False
End If

End Function

Function IsItHere(lp_strLocation As String) As Boolean

Dim l_blnIsIt As Boolean, l_varTemp As Variant
l_blnIsIt = False

If IsNumeric(lp_strLocation) Then
    
    'Numeric locations are job numbers - it is booked out to a job number - this is an internal location
    l_blnIsIt = True

Else
    
    l_varTemp = GetDataSQL("SELECT TOP 1 format FROM xref WHERE category = 'location' and description = '" & lp_strLocation & "';")
    Select Case l_varTemp
        'List of locations that are defined as 'Here' - not including the ones that will be going away
        Case "ONSITE"
            l_blnIsIt = True
        Case Else
            l_blnIsIt = False
    
    End Select

End If

IsItHere = l_blnIsIt

End Function

Function GetDataSQL(lp_strSQL As String) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open lp_strSQL, l_con, adOpenStatic, adLockReadOnly

    If l_rstGetData.RecordCount > 0 Then
        If Not IsNull(l_rstGetData.Fields(0)) Then
            l_rstGetData.MoveFirst
            GetDataSQL = l_rstGetData.Fields(0)
            GoTo Proc_CloseDB
        
        End If
    Else
        GoTo Proc_CloseDB
    End If

    Select Case l_rstGetData.Fields(0).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetDataSQL = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetDataSQL = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetDataSQL = 0
    Case Else
        GetDataSQL = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Function GetStatusNumber(lp_strStatus As String) As Integer
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_intStatusNumber As Integer
    Select Case LCase(lp_strStatus)
        Case "pencil"
            l_intStatusNumber = 0
        Case "submitted"
            l_intStatusNumber = 1
        Case "confirmed"
            l_intStatusNumber = 2
        Case "despatched"
            l_intStatusNumber = 2
        Case "prepared"
            l_intStatusNumber = 2
        Case "masters here"
            l_intStatusNumber = 3
        Case "scheduled"
            l_intStatusNumber = 4
        Case "in progress"
            l_intStatusNumber = 5
        Case "pending"
            l_intStatusNumber = 6
        Case "vt done"
            l_intStatusNumber = 7
        Case "completed"
            l_intStatusNumber = 8
        Case "hold cost"
            l_intStatusNumber = 9
        Case "costed"
            l_intStatusNumber = 10
        Case "sent to accounts", "no charge"
            l_intStatusNumber = 11
    End Select
    GetStatusNumber = l_intStatusNumber
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetCount(lp_strSQL As String) As Double

Screen.MousePointer = vbHourglass

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
End With

l_rstSearch.ActiveConnection = Nothing

If l_rstSearch.EOF Then
    GetCount = 0
Else

    If Not IsNull(l_rstSearch(0)) Then
        GetCount = Val(l_rstSearch(0))
    Else
        GetCount = 0
    End If
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

Screen.MousePointer = vbDefault

End Function

Function SanitiseBarcode(lp_strBarcode As String) As String
    
Dim l_strNewBarcode As String
l_strNewBarcode = Replace(lp_strBarcode, "/", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "\", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "?", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "*", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "&", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "#", "-")
'l_strNewBarcode = Replace(l_strNewBarcode, "%", "-")

SanitiseBarcode = l_strNewBarcode

Exit Function
    
End Function

Function DADCSanitiseBarcode(lp_strBarcode As String) As String
    
Dim l_strNewBarcode As String
l_strNewBarcode = Replace(lp_strBarcode, "/", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "\", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "?", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "*", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "&", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "#", "_")
'l_strNewBarcode = Replace(l_strNewBarcode, "%", "-")

DADCSanitiseBarcode = l_strNewBarcode

Exit Function
    
End Function


